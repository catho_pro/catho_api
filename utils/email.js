require("dotenv").config();
const nodemailer = require("nodemailer");
const pug = require("pug");
const htmlToText = require("html-to-text");

module.exports = class Email {
  constructor(info, url) {
    this.info = info;
    this.to = info.user.email;
    this.name = info.user.name;
    this.custName = info.custName;
    this.custPhone = info.custPhone;
    this.url = url;
    this.from = `AVS <${process.env.EMAIL_USERNAME}>`;
  }

  newTransport() {
    // if (process.env.NODE_ENV === 'production') {
    //   // Sendgrid
    //   return nodemailer.createTransport({
    //     service: 'SendGrid',
    //     auth: {
    //       user: process.env.SENDGRID_USERNAME,
    //       pass: process.env.SENDGRID_PASSWORD
    //     }
    //   });
    // }

    //send complaint to honeyasso78@gmail.com
    // if (this.to === "complaint.soddo@gmail.com" && this.name === "Complaint") {
    //   // console.log("yseral"); //TODO:

    //   return nodemailer.createTransport({
    //     host: process.env.EMAIL_HOST,
    //     port: process.env.EMAIL_PORT,
    //     auth: {
    //       user: process.env.EMAIL_COMPLAINT,
    //       pass: process.env.EMAIL_COMPLAINT_PASSWORD,
    //     },
    //   });
    // } else {
    return nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
    // }
  }

  // Send the actual email
  async send(template, subject, txt) {
    // 1) Render HTML based on a pug template
    // console.log("template", template);
    // console.log("subject", subject);
    // console.log("info", this.info);
    let html;
    if (!txt) {
      html = pug.renderFile(`${__dirname}/../utils/${template}.pug`, {
        name: this.name,
        url: this.url,
        subject,
      });
    } else {
      html = pug.renderFile(`${__dirname}/../utils/${template}.pug`, {
        name: this.name,
        custName: this.custName,
        custPhone: this.custPhone,
        url: this.url,
        subject,
        txt,
      });
    }

    // 2) Define email options
    const mailOptions = {
      from: this.from,
      to: this.to,
      subject,
      html,
      text: htmlToText.fromString(html),
    };

    // 3) Create a transport and send email
    try {
      await this.newTransport().sendMail(mailOptions);
    } catch (err) {
      console.log(err);
    }
  }

  // async sendWelcome() {
  //   await this.send('welcome', 'Welcome to the Natours Family!', null);
  // }

  async sendPasswordReset() {
    console.log("sendpasswordrest() func");

    await this.send(
      "passwordReset",
      "AVS Your password reset token (valid for only 15 minutes)",
      null
    );
  }

  async sendCustomEmail(subject, txt) {
    await this.send("reply", subject, txt);
  }
  async sendComplaint(subject, txt) {
    // console.log("why da faq");

    await this.send("complaint", subject, txt);
  }
};
