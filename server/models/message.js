"use strict";
const Email = require("../../utils/email");

module.exports = function (Message) {
  Message.sendEmail = async function (emailInfo) {
    console.log(emailInfo);
    if (emailInfo.user.email === "complaint.soddo@gmail.com" && emailInfo.user.name === "Complaint") {
      await new Email(emailInfo, "www.soddocatholic.com").sendComplaint(
        emailInfo.subject,
        emailInfo.text
      );
    } else {
      await new Email(emailInfo, "www.soddocatholic.com").sendCustomEmail(
        emailInfo.subject,
        emailInfo.text
      );
    }

    // return "sent";
  };
  Message.remoteMethod("sendEmail", {
    description: "Send email for user",
    accepts: {
      arg: "emailInfo",
      type: "Object",
      required: true,
      http: {
        source: "query",
      },
    },
    // returns: {
    //   arg: "status",
    //   type: "string",
    //   root: true,
    // },
    http: {
      verb: "get",
      path: "/send-email-for-Customer",
    },
  });
};
