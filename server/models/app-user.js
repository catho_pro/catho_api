"use strict";
const Email = require("../../utils/email");

module.exports = function (Appuser) {
  Appuser.on("resetPasswordRequest", async function (info) {
    // console.log(info);
    await new Email(
      info,
      `http://192.168.1.5:8888/admin/reset_password?reset_token=${info.accessToken.id}`
    ).sendPasswordReset();
  });
};
